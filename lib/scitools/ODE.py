"""
Deprecated module.
"""

raise ImportError("""
The scitools.ODE module is removed from scitools.
A more developed module is available from
https://github.com/hplgit/odespy
""")
